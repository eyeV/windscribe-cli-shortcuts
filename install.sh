#!/bin/bash
#REMOVE PREVIOUS VERSION:
echo "
-----------------------------------------------------------
| Заметка: посмотреть что делает код можно открыв sh файл |
-----------------------------------------------------------

Введите пароль суперпользователя для установки:"

pass='no' 		     #Записывает значение "no" в переменную "pass"
while [ "$pass" = "no" ]; do #Если в переменной pass есть значение "no", то цикл будет повторятся до тех пор пока переменная не сменится
su $ROOT -c'
{
rm    -rf			/usr/share/applications/Windscribe
rm    -rf 	   		/home/$USER/.local/share/applications/LibreWolf-Windscribe
sudo 			   	apt install libnotify-bin
} &> /dev/null' && pass='okay'  
#В случае ввода правильного пароля, и успешного выполнения комманды, меняет переменную на okay, что завершает цикл заданный ранее
done



#Удаление прошлых версий:
rm    -rf 	   		/home/$USER/.local/share/applications/LibreWolf-Windscribe &> /dev/null 
rm    -rf 	   		/home/$USER/.local/share/applications/scripts/Windscribe   &> /dev/null 
mkdir -p			/home/$USER/.local/share/applications/scripts/Windscribe
#Установка текущей версии:
cp    -r LibreWolf-Windscribe	/home/$USER/.local/share/applications/scripts/Windscribe
cp    -r icons			/home/$USER/.local/share/applications/scripts/Windscribe
cp    -r script		   	/home/$USER/.local/share/applications/scripts/Windscribe
cp    -r remove.sh		/home/$USER/.local/share/applications/scripts/Windscribe

#Запуск скрипта создания ярлыков:
chmod uo+rwx links-create.sh
./links-create.sh

#Разрешение запуска
chmod -R uo+rwx 	/home/$USER/.local/share/applications/scripts/Windscribe
chmod -R uo+rwx 	/home/$USER/.local/share/applications/scripts/Windscribe/LibreWolf-Windscribe


echo "
---------------------------------
| Ярлыки Windscribe Установлены |
---------------------------------
"
echo "
Версия SysUpdate которая перед обновлением отключает Windscribe

1 - Версия которая обновляет пакеты только со свободных репозиториев
2 - Версия которая включает проприетарные репозитории до обновления и отключает после

Обе версии одинаково совместимы с Windscribe
Если у вас стоят драйвера от Nvidia, выбирайте вторую версию

"

cd SysUpdateW/
PS3="Введите число: "
select choice in "Свободная версия SysUpdate" "Проприетарная версия SysUpdate" "Выход (Не устанавливать SysUpdate)"; do

	case $choice in
	"Свободная версия SysUpdate")
	chmod -R u+rwx install.sh
	echo "
	"
	./install.sh
	break
	;;
	
	"Проприетарная версия SysUpdate")
	chmod -R u+rwx proprietary-install.sh
	echo "
	"
	./proprietary-install.sh;
	break
	;;
	
	"Выход (Не устанавливать SysUpdate)")
	echo "Выбран выход"
	break
	;;
esac
done


