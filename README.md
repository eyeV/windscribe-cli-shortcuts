## Windscribe VPN CLI Shortcuts

![](https://notabug.org/eyeV/windscribe-cli-shortcuts/raw/master/icons/A-Screenshot.png)


### GUI (Графическая версия) Windscribe VPN не работает в Devuan
Этот bash скрипт установит ярлыки для CLI версии приложения  
Для этого вам уже нужна установленная CLI версия Windscribe VPN которую можно скачать с официального сайта по ссылке:

https://rus.windscribe.com/guides/linux  

"Download Legacy CLI"  

### Cмена DNS
**Обязательно поменяйте DNS настройки вашего браузера на Windscribe DNS, иначе провайдер будет знать какие сайты вы посещаете!!!**

В настройках вашего браузера в поле "DNS Over Https" вставте адрес:  
https://freedns.controld.com/p0

Источник:  
https://controld.com/free-dns?freeResolverType=unfiltered

### Установка:

chmod +x install.sh  
./install.sh  

### Ярлыки и иконки можно редактировать как захотите

Если вам не нужны некоторые ярлыки, и вы хотите их удалить, сотрите их из скрипта "links-create.sh", потом запустите установку install.sh еще раз

### Исправление ошибки установки GUI версии

Если вы установили GUI версию Windscribe, она не работает и вы не можете ее удалить /  
Ошибка Windscribe helper initialize error. Please reinstall the application or contact support /  
Система не обновляется, вот решение:

```
su  
cd /var/lib/dpkg/info  
sudo rm -r windscribe.list windscribe.md5sums windscribe.postinst windscribe.preinst windscribe.prerm  
apt remove -y windscribe  
rm -r /usr/local/windscribe  
rm -r /usr/share/applications/windscribe.desktop  
sudo dpkg --configure -a  
```