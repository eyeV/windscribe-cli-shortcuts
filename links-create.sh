#!/bin/bash

#Менеджмент VPN:
#Создаем папку для ярлыков менеджмента:
mkdir -p 	/home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Manage

# VPN Disconnect
echo "[Desktop Entry]
Type=Application
Name=VPN Disconnect
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/VPN-Disconnect.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/Manage/VPN-Disable.sh
Terminal=false
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Manage/VPN-Disable.desktop

# Firewall Status
echo "[Desktop Entry]
Type=Application
Name=Firewall Status
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Firewall-Status.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/Manage/Firewall-Status.sh
Terminal=false
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Manage/Firewall-Status.desktop

# Firewall Disable
echo "[Desktop Entry]
Type=Application
Name=Firewall Disable
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Firewall-Disable.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/Manage/Firewall-Disable.sh
Terminal=false
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Manage/Firewall-Disable.desktop


#Установка ярлыка для запуска LibreWolf с Windscribe:
mkdir -p			/home/$USER/.local/share/applications/scripts/Windscribe/LibreWolf-Windscribe

echo "[Desktop Entry]
Name=Windscribe LibreWolf
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/LibreWolf-Windscribe/start-new-window.sh
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/LibreWolf-Windscribe/windscribe.png
Type=Application
StartupWMClass=LibreWolf
Categories=Network;WebBrowser;VPN;Windscribe;
StartupNotify=true
Terminal=false
X-MultipleArgs=false
Keywords=Internet;WWW;Browser;Web;Explorer;VPN;Windscribe;
Actions=new-window;new-private-window;profilemanager;
X-Flatpak=io.gitlab.librewolf-community

[Desktop Action new-window]
Name=Open a New Window
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/LibreWolf-Windscribe/start-new-window.sh

[Desktop Action new-private-window]
Name=Open a New Private Window
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/LibreWolf-Windscribe/start-private-window.sh

[Desktop Action profilemanager]
Name=Open the Profile Manager
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/LibreWolf-Windscribe/start-profile-manager.sh" >> /home/$USER/.local/share/applications/scripts/Windscribe/LibreWolf-Windscribe/LibreWolfW.desktop

#Countries:

# Canada
echo "[Desktop Entry]
Type=Application
Name=Canada
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/Canada.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/Canada.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Canada.desktop

# Canada West
echo "[Desktop Entry]
Type=Application
Name=Canada West
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/Canada.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/Canada West.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Canada-West.desktop

# France
echo "[Desktop Entry]
Type=Application
Name=France
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/France.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/France.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/France.desktop

# Germany
echo "[Desktop Entry]
Type=Application
Name=Germany
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/Germany.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/Germany.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Germany.desktop

# Hong Kong
echo "[Desktop Entry]
Type=Application
Name=Hong Kong
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/Hong Kong.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/Hong Kong.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Hong-Kong.desktop

# Netherlands
echo "[Desktop Entry]
Type=Application
Name=Netherlands
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/Netherlands.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/Netherlands.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Netherlands.desktop

# Norway
echo "[Desktop Entry]
Type=Application
Name=Norway
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/Norway.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/Norway.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Norway.desktop

# Romania
echo "[Desktop Entry]
Type=Application
Name=Romania
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/Romania.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/Romania.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Romania.desktop

# Switzerland
echo "[Desktop Entry]
Type=Application
Name=Switzerland
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/Switzerland.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/Switzerland.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Switzerland.desktop

# Turkey
echo "[Desktop Entry]
Type=Application
Name=Turkey
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/Turkey.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/Turkey.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/Turkey.desktop

# United Kingdom
echo "[Desktop Entry]
Type=Application
Name=United Kingdom
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/United Kingdom.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/United Kingdom.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/United-Kingdom.desktop

# US
echo "[Desktop Entry]
Type=Application
Name=US
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/US.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/US.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/US.desktop

# US Central
echo "[Desktop Entry]
Type=Application
Name=US Central
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/US.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/US Central.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/US-Central.desktop

# US West
echo "[Desktop Entry]
Type=Application
Name=US West
Icon=/home/$USER/.local/share/applications/scripts/Windscribe/icons/Countries/US.png
Exec=/home/$USER/.local/share/applications/scripts/Windscribe/script/US West.sh
Terminal=true
Keywords=Windscribe;VPN;" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/US-West.desktop

# 
#echo "" >> /home/$USER/.local/share/applications/scripts/Windscribe/windscribe-links/.desktop
