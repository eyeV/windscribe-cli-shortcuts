#!/bin/bash

fstatus=`windscribe firewall`

if [[ $fstatus =~ "Firewall mode: on" ]]; then
 notify-send "Firewall Включен"
else
 notify-send "Firewall Отключен"
fi
