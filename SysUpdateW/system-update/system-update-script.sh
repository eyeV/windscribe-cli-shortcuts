## ОТКЛЮЧЕНИЕ VPN ПЕРЕД УСТАНОВКОЙ ОБНОВЛЕНИЙ

#Останавливает Браузер LibreWolf (предотвращение утечки данных в случае если браузер запущен с VPN)
{
killall librewolf
killall GeckoMain
killall bwrap
killall WebContent
killall WebExtensions
killall PrivilegedCont
} &> /dev/null
echo "Браузер LibreWolf Остановлен"

#Отключает Windscribe VPN
windscribe firewall auto    	&>/dev/null
windscribe disconnect       	&>/dev/null
echo "VPN Отключен"



## ОБНОВЛЕНИЕ СИСТЕМЫ:

echo "Введите пароль суперпользователя для обновления системы"

pass='no' 		     #Записывает значение "no" в переменную "pass"
while [ "$pass" = "no" ]; do #Если в переменной pass есть значение "no", то цикл будет повторятся до тех пор пока переменная не сменится
su root -c'		     #Запускает следующие комманды от имени root

#Обновление:
echo "Подготовка обновления"
apt update 			 &>/dev/null; 		echo "Репозитории обновлены";
apt upgrade -y				    ;
apt -y dist-upgrade 		 &>/dev/null;		echo "Чистка не используемых пакетов";
apt clean 			 &>/dev/null;		
apt -y autoremove 		 &>/dev/null;
echo "Обновление выполнено"
' && pass='okay' && notify-send "Обновление выполнено"
#В случае ввода правильного пароля, и успешного выполнения комманды обновления, меняет переменную на okay, что завершает цикл заданный ранее
#Также присылает уведомление об обновлении
done

echo "Для выхода нажмите Enter
"
read
