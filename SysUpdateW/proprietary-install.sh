#!/bin/bash

##Проприетарный скрипт обновления (отличается только тем что включает несвободные репозитории перед обновлением и выключает после)

echo "Установка Проприетарной Версии SysUpdate

Введите пароль суперпользователя для удаления прошлых версий SysUpdate"

#Удаление всех возможных предыдущих версий этого скрипта
pass='no' 		     #Записывает значение "no" в переменную "pass"
while [ "$pass" = "no" ]; do #Если в переменной pass есть значение "no", то цикл будет повторятся до тех пор пока переменная не сменится
su root -c'{
rm -rf /usr/share/applications/system-update-proprietary
rm -rf /usr/share/applications/system-update
rm -rf /home/$USER/.local/share/applications/system-update-proprietary
rm -rf /home/$USER/.local/share/applications/system-update
rm -rf /home/$USER/.local/share/applications/scripts/system-update-proprietary
rm -rf /home/$USER/.local/share/applications/scripts/system-update
} &> /dev/null' && pass='okay' && echo "Прошлые версии удалены
"
#В случае ввода правильного пароля, и успешного выполнения комманды, меняет переменную на okay, что завершает цикл заданный ранее
done

#Создание папки для скриптов
mkdir -p				/home/$USER/.local/share/applications/scripts

#Копирование папки со скриптом
cp    -r system-update-proprietary	/home/$USER/.local/share/applications/scripts

#Создание ярлыка
echo "[Desktop Entry]
Type=Application
Name=SysUpdate
Icon=/home/$USER/.local/share/applications/scripts/system-update-proprietary/system-update.png
Exec=/home/$USER/.local/share/applications/scripts/system-update-proprietary/system-update-script.sh
Terminal=true" >> /home/$USER/.local/share/applications/scripts/system-update-proprietary/system-update.desktop

#Разрешение прав на запуск:
chmod -R uo+rwx 			/home/$USER/.local/share/applications/scripts/system-update-proprietary

echo "Версия SysUpdate с проприетарными обновлениями установлена"
