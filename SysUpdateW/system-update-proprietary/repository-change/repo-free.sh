##Удаляет две тры и четыре решетки из репозиториев (исправляет нагромождение решеток при смене):
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
###:
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
####:
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list

## МЕНЯЕТ РЕПОЗИТОРИИ ОБРАТНО НА СВОБОДНЫЕ (убирает решетки перед свободными и ставит перед несвободными)

#Devuan 4.0 Chimaera (stable) (Включает)
sed -i  's|#deb     http://deb.devuan.org/merged chimaera           main|deb     http://deb.devuan.org/merged chimaera           main|g' /etc/apt/sources.list
sed -i  's|#deb     http://deb.devuan.org/merged chimaera-security  main|deb     http://deb.devuan.org/merged chimaera-security  main|g' /etc/apt/sources.list
sed -i  's|#deb     http://deb.devuan.org/merged chimaera-updates   main|deb     http://deb.devuan.org/merged chimaera-updates   main|g' /etc/apt/sources.list
sed -i  's|#deb     http://deb.devuan.org/merged chimaera-security  main|deb     http://deb.devuan.org/merged chimaera-security  main|g' /etc/apt/sources.list
sed -i  's|#deb-src http://deb.devuan.org/merged chimaera           main|deb-src http://deb.devuan.org/merged chimaera           main|g' /etc/apt/sources.list
sed -i  's|#deb-src http://deb.devuan.org/merged chimaera-updates   main|deb-src http://deb.devuan.org/merged chimaera-updates   main|g' /etc/apt/sources.list
sed -i  's|#deb-src http://deb.devuan.org/merged chimaera-security  main|deb-src http://deb.devuan.org/merged chimaera-security  main|g' /etc/apt/sources.list

#Devuan 3.1   Beowulf (Для зависимостей) (Включает)
sed -i  's|#deb 	http://deb.devuan.org/merged beowulf          	main|deb 	http://deb.devuan.org/merged beowulf          	main|g' /etc/apt/sources.list
sed -i  's|#deb-src http://deb.devuan.org/merged beowulf          	main|deb-src http://deb.devuan.org/merged beowulf          	main|g' /etc/apt/sources.list

#Devuan 2.1   ASCII   (Для зависимостей) (Включает)
sed -i  's|#deb 	http://deb.devuan.org/merged ascii          	main|deb 	http://deb.devuan.org/merged ascii          	main|g' /etc/apt/sources.list
sed -i  's|#deb-src http://deb.devuan.org/merged ascii          	main|deb-src http://deb.devuan.org/merged ascii          	main|g' /etc/apt/sources.list

#Devuan 1.0.0 Jessie  (Для зависимостей) (Включает)
sed -i  's|#deb 	http://archive.devuan.org/merged jessie         main|deb 	http://archive.devuan.org/merged jessie         main|g' /etc/apt/sources.list
sed -i  's|#deb-src http://archive.devuan.org/merged jessie         main|deb-src http://archive.devuan.org/merged jessie         main|g' /etc/apt/sources.list

#NON FREE (НЕ СВОБОДНЫЕ, ВКЛЮЧАТЬ ТОЛЬКО ЕСЛИ ВСЕ ЧТО ВЫШЕ ЗАКОММЕНТИРОВАНЫ) (Выключает)
sed -i  's|deb      http://deb.devuan.org/merged chimaera          main contrib non-free|#deb      http://deb.devuan.org/merged chimaera          main contrib non-free|g' /etc/apt/sources.list
sed -i  's|deb-src  http://deb.devuan.org/merged chimaera          main contrib non-free|#deb-src  http://deb.devuan.org/merged chimaera          main contrib non-free|g' /etc/apt/sources.list
sed -i  's|deb 	  http://deb.devuan.org/merged beowulf          	main contrib non-free|#deb 	  http://deb.devuan.org/merged beowulf           main contrib non-free|g' /etc/apt/sources.list
sed -i  's|deb-src  http://deb.devuan.org/merged beowulf           main contrib non-free|#deb-src  http://deb.devuan.org/merged beowulf           main contrib non-free|g' /etc/apt/sources.list
sed -i  's|deb 	  http://deb.devuan.org/merged ascii          	 main contrib non-free|#deb 	  http://deb.devuan.org/merged ascii          	 main contrib non-free|g' /etc/apt/sources.list
sed -i  's|deb-src  http://deb.devuan.org/merged ascii          	 main contrib non-free|#deb-src  http://deb.devuan.org/merged ascii          	 main contrib non-free|g' /etc/apt/sources.list
sed -i  's|deb 	  http://archive.devuan.org/merged jessie	 main contrib non-free|#deb 	  http://archive.devuan.org/merged jessie	 main contrib non-free|g' /etc/apt/sources.list
sed -i  's|deb-src  http://archive.devuan.org/merged jessie	 main contrib non-free|#deb-src  http://archive.devuan.org/merged jessie	 main contrib non-free|g' /etc/apt/sources.list







##Удаляет две тры и четыре решетки из репозиториев (исправляет нагромождение решеток при смене):
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
sed -i  's|##|#|g' /etc/apt/sources.list
###:
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
sed -i  's|###|#|g' /etc/apt/sources.list
####:
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list
sed -i  's|####|#|g' /etc/apt/sources.list

#Обновляет список репозиториев в системе
apt update
